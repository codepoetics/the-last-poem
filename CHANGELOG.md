# Changelog

## 0.1.1 - 2022-07-31

### Added

* Infer cyborg version of poem.
* Backspace in machine version of poem, only over changed words.
* Add machine styling.

### Changed

* Show "Recite" button only when no process is active.
* Refine machine poem.
* Pull JS and CSS into separate files.

## 0.1.0 - 2022-07-29

### Added

* Draft human poem.
* Draft machine poem.
* Provide "Recite button".
* Add typed animation using [typed.js](https://github.com/mattboldt/typed.js).
* Do some basic styling.
