const body = document.querySelector("body");
const humanLines = document.querySelector("#human-lines");
const machineLines = document.querySelector("#machine-lines");
const cyborgLines = document.querySelector("#cyborg-lines");
const byline = document.querySelector("#byline");
const buttons = document.querySelector("#buttons");

let humanTyped;
let machineTyped;

const showByline = () => {
  byline.style.display = "block";
};
const hideByline = () => {
  byline.style.display = "none";
};
const showHuman = () => {
  humanLines.style.display = "block";
};
const hideHuman = () => {
  humanLines.style.display = "none";
};
const showMachine = () => {
  machineLines.style.display = "block";
};
const hideMachine = () => {
  machineLines.style.display = "none";
};
const showCyborg = () => {
  cyborgLines.style.display = "block";
};
const hideCyborg = () => {
  cyborgLines.style.display = "none";
};
const setHumanView = () => {
  body.classList.remove("machine");
  body.classList.remove("cyborg");
  body.classList.add("human");
};
const setMachineView = () => {
  body.classList.remove("human");
  body.classList.remove("cyborg");
  body.classList.add("machine");
};
const setCyborgView = () => {
  body.classList.remove("human");
  body.classList.remove("machine");
  body.classList.add("cyborg");
};
const showButtons = () => {
  buttons.style.display = "block";
};
const hideButtons = () => {
  buttons.style.display = "none";
};
const humanOptions = {
  stringsElement: '#human-lines',
  startDelay: 10,
  typeSpeed: 60,
  backDelay: 1500,
  onComplete: () => {
    setTimeout(() => {
      humanTyped.destroy();
      showHuman();
      setTimeout(() => {
        if (!machineTyped) {
          machineTyped = new Typed('#machine-terminal', machineOptions);
        }
        hideByline();
        hideHuman();
        setMachineView();
        machineTyped.reset();
      }, 5000);
    }, 3000);
  }
};
const machineOptions = {
  stringsElement: '#machine-lines',
  typeSpeed: 90,
  onComplete: () => {
    setTimeout(() => {
      machineTyped.destroy();
      showMachine();
      setTimeout(() => {
        hideMachine();
        setCyborgView();
        showCyborg();
        showButtons();
      }, 5000);
    }, 3000);
  }
};
document.querySelector('#reset').addEventListener('click', () => {
  if (!humanTyped) {
    humanTyped = new Typed('#human-terminal', humanOptions);
  }
  showByline();
  hideButtons();
  hideCyborg();
  setHumanView();
  humanTyped.reset();
});
