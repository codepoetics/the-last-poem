# The Last Poem

An experiment in code + poetry.

## Origins

Written for [Technoculture, an online journal of technology in society](https://tcjournal.org/).

See also the [author's website](https://nedjo.ca/).

## Installation

This project uses [Typed.js](https://mattboldt.com/demos/typed-js/), by [Matt Boldt](https://mattboldt.com/) and other contributors.

To install, run `npm install`.

Alternately:

* Create a directory, `node_modules`, in the root directory of this repository.
* [Download Typed.js](https://github.com/mattboldt/typed.js/releases)
* Extract the download into the `node_modules` directory.
* Rename the extracted directory to `typed.js` (it will originally be called something like `typed.js-2.0.12`).
* Optionally, delete everything that's in the `node_modules/typed.js` directory except the file `node_modules/typed.js/dist/typed.umd.js`.

## Licenses

* Poetry is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
* Code is licensed under the [GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.en.html).
